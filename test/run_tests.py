#!/usr/bin/env python3
# Copyright (C) 2020 Bitcoin Cash developers
# Distributed under the MIT software license, see the accompanying
# file COPYING or http://www.opensource.org/licenses/mit-license.php.

from bitcoincash.core import b2lx
import argparse
import bitcoincash.rpc
import bitcoincash
import logging
import os
import random
import sys
import tempfile
import threading
import time

ABC_IFP_ADDRESS = 'bchreg:pqnqv9lt7e5vjyp0w88zf2af0l92l8rxdgd35g0pkl'
ACTIVATION_TIME = 1605441600
ROOT_DIR = os.path.realpath(os.path.join(os.path.dirname(__file__), os.pardir))
IFPGUARD_PATH = os.path.join(ROOT_DIR, "ifpguard.py")

bitcoincash.SelectParams('regtest')

parser = argparse.ArgumentParser(description="IFP Guard tests")
parser.add_argument('bitcoind', type=str, help="Path to bitcoind")
parser.add_argument('--debug', action='store_true', help="Show debug output")
parser.add_argument('--bu', action='store_true', help="bitcoind is Bitcoin Unlimited")
parser.add_argument('--bchn', action='store_true', help="bitcoind is Bitcoin Cash Node")

progargs = parser.parse_args()

loglevel = logging.DEBUG if progargs.debug else logging.INFO


logging.basicConfig(
        stream=sys.stdout,
        format='%(asctime)s,%(msecs)d %(name)s %(levelname)s %(message)s',
        datefmt='%H:%M:%S',
        level=loglevel)

if (not progargs.bchn and not progargs.bu) or (progargs.bchn and progargs.bu):
    logging.error("You need to specify --bchn OR --bu")
    sys.exit(1)

##
# This test has 3 nodes.
# - Node 0 does not have IFP guard.
# - Node 1 and 2 use IFP guard.
def run_tests(nodes):
    logging.info("Starting tests")
    logging.info("node0 does not have IFP guard.")
    logging.info("node1 and node2 have IFP guard.")
    rpc_0 = nodes[0]['rpc']
    rpc_1 = nodes[1]['rpc']
    rpc_2 = nodes[2]['rpc']

    # Connect nodes 0 and 1
    rpc_0.addnodeonetry("localhost:{}".format(nodes[1]['port']))

    # Generate 20 blocks. Observe that node 1 follows node 0.
    rpc_0.generate(20)
    wait_until(lambda: rpc_0.getbestblockhash() == rpc_1.getbestblockhash())

    # Set time to network upgrade
    for n in nodes:
        n['rpc'].call("setmocktime", ACTIVATION_TIME)

    # Mine 5 blocks. This puts MTP before activation time.
    rpc_0.generate(5)
    wait_until(lambda: rpc_0.getbestblockhash() == rpc_1.getbestblockhash())
    logging.info("pre-fork node0 and node1 are at same chain tip - OK")

    assert_mediantime(rpc_0, lambda mtp: mtp < ACTIVATION_TIME)

    # Node 1 mines the activation block to ABC's IFP address. Node 0 should reject.
    rpc_0.generatetoaddress(1, ABC_IFP_ADDRESS)
    assert_mediantime(rpc_0, lambda mtp: mtp == ACTIVATION_TIME)

    logging.info("network upgrade, node0 mines IFP block")
    # Observe that node 1 rejected the IFP block.
    time.sleep(5)
    assert(rpc_0.getblockcount() == 26)
    assert(rpc_1.getblockcount() == 25)

    # Assert that node 1 sees the invalid IFP chain
    chaintips = rpc_1.call("getchaintips")
    ifptip = [t for t in chaintips if t['height'] == 26]
    assert(len(ifptip) == 1)
    ifptip = ifptip.pop()
    assert(ifptip['status'] == "invalid")
    assert(ifptip['hash'] == b2lx(rpc_0.getbestblockhash()))
    logging.info("node1 rejected IFP block - OK")

    # Node mine 50 more blocks with node 0 (the invalid chain) and 2 more with
    # node 1.
    #
    # Then connect node 2 and observe it follows node 1, even though it has
    # less chain work.
    logging.info("node0 mines the longest chain")
    rpc_0.generate(50)
    assert(rpc_0.getblockcount() == 26 + 50)
    rpc_1.generate(10)
    assert(rpc_1.getblockcount() == 25 + 10)
    rpc_2.addnodeonetry("localhost:{}".format(nodes[0]['port']))
    rpc_2.addnodeonetry("localhost:{}".format(nodes[1]['port']))
    wait_until(lambda: rpc_2.getbestblockhash() == rpc_1.getbestblockhash())
    logging.info("node2 connects to the network, accepts node1's chain - OK")

    # Verify that node 2 also sees the invalid IFP chain
    chaintips = rpc_2.call("getchaintips")
    ifptip = [t for t in chaintips if t['height'] == rpc_0.getblockcount()]
    assert(len(ifptip) == 1)
    ifptip = ifptip.pop()
    assert(ifptip['status'] != "active")
    assert(ifptip['hash'] == b2lx(rpc_0.getbestblockhash()))
    logging.info("node2 rejects node0's chain - OK")

def assert_mediantime(rpc, test):
    tip = rpc.getblockheader(rpc.getbestblockhash(), True)
    assert(test(tip['mediantime']))

def wait_until(predicate, timeout = 5):
    start = time.time()
    while True:
        if predicate():
            return

        if time.time() > start + timeout:
            raise Exception("timeout")

def output_reader(pipe, queue):
    try:
        with pipe:
            for l in iter(pipe.readline, b''):
                queue.put(l)
    finally:
        queue.put(None)

def bail(*args):
    logging.error(*args)
    sys.exit(1)

def run_cmd(cmd, logprefix = ""):
    import subprocess
    from threading import Thread
    from queue import Queue

    logging.info("Running {}".format(cmd))
    p = subprocess.Popen(cmd,
            stdout = subprocess.PIPE,
            stderr = subprocess.PIPE)
    q = Queue()
    Thread(target = output_reader, args = [p.stdout, q]).start()
    Thread(target = output_reader, args = [p.stderr, q]).start()

    for line in iter(q.get, None):
        logging.debug("{}: {}".format(
            logprefix,
            line.decode('utf-8').rstrip()))

    p.wait()
    rc = p.returncode
    assert rc is not None
    if rc != 0:
        bail("Command {} failed with return code {}".format(cmd, rc))

def init_bitcoind_dir(uses_ifpguard, node):
    port = node['port']
    rpcport = node['rpcport']

    tmpdir = tempfile.TemporaryDirectory()

    blocknotify = None
    if uses_ifpguard:
        blocknotify = "{} --regtest --bitcoind-config-file={}".format(
                IFPGUARD_PATH, os.path.join(tmpdir.name, "bitcoin.conf"))

    with open(os.path.join(tmpdir.name, "bitcoin.conf"), 'w', encoding='utf-8') as fh:
        fh.write("debug=1\n")
        fh.write("regtest=1\n")

        if blocknotify is not None:
            fh.write("blocknotify={}\n".format(blocknotify))

        if progargs.bu:
            fh.write("printtoconsole=1\n")
            fh.write("keypool=10\n")

        if progargs.bchn:
            fh.write("[regtest]\n")

        fh.write("rpcport={}\n".format(rpcport))
        fh.write("port={}\n".format(port))


    return tmpdir

def start_bitcoind(datadir, nodeid):
    run_cmd([ progargs.bitcoind, "-datadir={}".format(datadir.name) ], logprefix=nodeid)


def random_port():
    return random.randint(10000, 50000)

def main():
    # Setup nodes
    node0 = { 'rpcport': random_port(), 'port': random_port() }
    node1 = { 'rpcport': random_port(), 'port': random_port() }
    node2 = { 'rpcport': random_port(), 'port': random_port() }

    node0['datadir'] = init_bitcoind_dir(uses_ifpguard = False, node = node0)
    node1['datadir'] = init_bitcoind_dir(uses_ifpguard = True, node = node1)
    node2['datadir'] = init_bitcoind_dir(uses_ifpguard = True, node = node2)

    node0['process'] = threading.Thread(target = start_bitcoind, args = [ node0['datadir'], "node0" ])
    node1['process'] = threading.Thread(target = start_bitcoind, args = [ node1['datadir'], "node1" ])
    node2['process'] = threading.Thread(target = start_bitcoind, args = [ node2['datadir'], "node2" ])

    nodes = [node0, node1, node2]

    for n in nodes:
        n['process'].start()

    logging.info("Waiting for bitcoind to start...")
    time.sleep(5)

    node0['rpc'] = bitcoincash.rpc.Proxy(
            btc_conf_file = os.path.join(node0['datadir'].name, "bitcoin.conf"))
    node1['rpc'] = bitcoincash.rpc.Proxy(
            btc_conf_file = os.path.join(node1['datadir'].name, "bitcoin.conf"))
    node2['rpc'] = bitcoincash.rpc.Proxy(
            btc_conf_file = os.path.join(node2['datadir'].name, "bitcoin.conf"))

    try:
        run_tests(nodes)
    finally:
        # Finish
        for n in nodes:
            n['rpc'].call('stop')
            # Allow all `ifpguard.py` calls to complete
            time.sleep(5)
        for n in nodes:
            n['process'].join()

main()
