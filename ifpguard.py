#!/usr/bin/env python3
# Copyright (C) 2020 Bitcoin Cash developers
# Distributed under the MIT software license, see the accompanying
# file COPYING or http://www.opensource.org/licenses/mit-license.php.

from bitcoincash.core import b2lx, lx
from bitcoincash.wallet import CBitcoinAddress
import argparse
import bitcoincash.cashaddr
import bitcoincash.rpc
import logging
import sys

PROJECT_NAME="IFP Guard"

description = "{} - Guard the chain against the IFP soft fork by invalidating its first block.".format(PROJECT_NAME)

parser = argparse.ArgumentParser(description=description)
parser.add_argument('--axionactivationtime', default=1605441600, help="Upgrade activation time")
parser.add_argument('--bitcoind-config-file', default=None, help="Path to bitcoin.conf")
parser.add_argument('--regtest', action='store_true', help="If bitcoind is running on regtest")
parser.add_argument('--testnet', action='store_true', help="If bitcoind is running on testnet")
parser.add_argument('--logfile', default="ifpguard.log", help="Path to logfile")

args = parser.parse_args()

logging.basicConfig(
        filename=args.logfile,
        filemode='a',
        format='%(asctime)s,%(msecs)d %(name)s %(levelname)s %(message)s',
        datefmt='%H:%M:%S',
        level=logging.DEBUG)

log = logging.getLogger('ifpguard')
# Also log to terminal
log.addHandler(logging.StreamHandler(sys.stdout))

(_, IFP_ADDRESS_KIND, IFP_ADDRESS_HASH) = bitcoincash.cashaddr.decode(
        "bitcoincash:pqnqv9lt7e5vjyp0w88zf2af0l92l8rxdgnlxww9j9")


if args.testnet and args.regtest:
    log.error("Both testnet and regtest cannot be enabled at the same time.")
    sys.exit(1)

if args.regtest:
    bitcoincash.SelectParams('regtest')
if args.testnet:
    bitcoincash.SelectParams('testnet')

rpc = bitcoincash.rpc.Proxy(
        btc_conf_file = args.bitcoind_config_file)

def upgrade_has_activated(mtp) -> bool:
    return mtp >= args.axionactivationtime

def get_median_time_past(blockhash) -> int:
    header = get_blockheader_verbose(blockhash)
    return header['mediantime']

def contains_ifp_output(blockhash) -> bool:
    """
    Checks if the block contains ABC IFP output.
    """
    if isinstance(blockhash, str):
        blockhash = lx(blockhash)
    coinbase = rpc.getblock(blockhash).vtx[0]

    for out in coinbase.vout:
        address = str(CBitcoinAddress.from_scriptPubKey(out.scriptPubKey))
        (_, kind, hash) = bitcoincash.cashaddr.decode(address)

        if (kind == IFP_ADDRESS_KIND and hash == IFP_ADDRESS_HASH):
            return True

    return False

def get_blockheader_verbose(blockhash):
    if not isinstance(blockhash, str):
        blockhash = b2lx(blockhash)
    return rpc.call("getblockheader", blockhash, True)

def locate_upgrade_block(blockhash_tip):
    """
    Locate the first block after the network upgrade.

    We start at the tip and walk backward until we find the first block.
    """
    if not isinstance(blockhash_tip, str):
        blockhash_tip = b2lx(blockhash_tip)
    upgrade_block = blockhash_tip
    prev_block_hash = get_blockheader_verbose(upgrade_block)['previousblockhash']

    while True:
        prev_header = get_blockheader_verbose(prev_block_hash)

        if not upgrade_has_activated(prev_header['mediantime']):

            # This block's mtp is before activation. This means the previous
            # block we walked through was the upgrade block.

            log.info("Located network upgrade block {}".format(upgrade_block))
            assert(upgrade_has_activated(get_median_time_past(upgrade_block)))
            return upgrade_block

        # This block was mined after the network upgrade and is the oldest one
        # we've found so far. Flag as upgrade block.

        upgrade_block = prev_header['hash']
        prev_block_hash = prev_header['previousblockhash']

    assert(False) # should never get here


def main():
    log.info("{} running...".format(PROJECT_NAME))
    chaintip = rpc.getbestblockhash()
    log.info("Blockhain tip is {}".format(b2lx(chaintip)))
    mtp = get_median_time_past(chaintip)

    if (mtp < args.axionactivationtime):
        log.info("November 2020 network upgrade has not activated yet.")
        log.info("Upgrade MTP time: {}, blockchain tip MTP time: {}".format(
                args.axionactivationtime, mtp))
        return

    log.info("November 2020 network upgrade is active.")
    blockhash = locate_upgrade_block(chaintip)

    if contains_ifp_output(blockhash):
        log.info("!! Upgrade block {} contains IFP output !!"
                .format(blockhash))
        log.info("!! Rejecting block {} !!".format(blockhash))
        rpc.call("invalidateblock", blockhash)
    else:
        log.info("OK! Upgrade block {} does not contain IFP output."
            .format(blockhash))

main()

