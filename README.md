# IFP Guard - completely reject the IFP

## Background

In Nov 2020, Bitcoin ABC will attempt to change the protocol rules of
Bitcoin Cash, so that 8% of the miner award goes to ABC, rather than the
miners that secure the network. They call this new rule "the IFP".

ABC implemented this IFP rule like a soft-fork. Bitcoin ABC will reject
blocks made by other nodes, while other nodes will not reject blocks made by
Bitcoin ABC.

The purpose of implementing their IFP as a soft-fork is strategic:

Even if ABC has less hash power, their hash power minority may get lucky at
solving blocks. If this happens, without intervention, some non-ABC mined
blocks are at risk of being orphaned in a block reorg. This threat is
intimidating to miners, who risk the most in the short term. This script is
intended to mitigate this threat.

## A stategy to reject IFP

A simple rule can be applied to reject "the IFP". It is as follows:

> The FIRST block after the protocol upgrade activation SHALL NOT
> have an output to Bitcoin ABC's IFP address in the coinbase
> transaction.

This simple additional rule unites miners on a single strategy to defeat the
IFP chain.

Nodes that are configured with this script will enforce this rule.

Using this script helps level the playing field. When the non-ABC miners
enforce this additional rule, either using this script or with manual
intervention, they are no longer at risk of having blocks orphaned by luck of
ABC miners.

You might say this script cuts of the head off the "IFP snake".

## Why enforce a rule via script?

Hard coding a rule like this into the full node would set a bad precedent that
the network is capable of centrally dictated censorship of addresses and
transactions.  However, by making this an opt-in, adjunct script we simply
offer miners tools to reject IFP, rather than dictating and enforcing such a
rule.

### How does this script work?

When a new block is detected, it does as follows:

1. Looks at the nodes current blockchain tip.
1. From the tip, it traverses the blockchain backward to find the FIRST block
   after November 2020 upgrade.
1. If the coinbase of this FIRST block sends funds to the ABC IFP address,
   the block is rejected.

This approach works because ABC nodes via their soft-fork will do the opposite.
They will reject a chain where the FIRST block does not send funds to the ABC
IFP address.

## Installation

```
pip3 install -r requirements.txt --user
```

### Run tests

This script is tested with both Bitcoin Unlimited and Bitcoin Cash Node.

To run tests with Bitcoin Cash Node:
```
./test/run_tests.py <path to bitcoind> --bchn
```

To run tests with Bitcoin Unlimited:
```
./test/run_tests.py <path to bitcoind> --bu
```

## Configuration

This script integrates with bitcoind's `blocknotify` option. The `blocknotify`
parameter is set in the `bitcoin.conf` file.

Run `./ifpguard.py --help` to see all options.

### Mainnet example

If your `bitcoin.conf` file is in the default location, add the following to
`bitcoin.conf`
```
blocknotify=/some/path/ifpguard/ifpguard.py
```

If your `bitcoin.conf` is in a custom location, IFPGuard needs to know. Pass
the location with `--bitcoind-config-file`.

IFPGuard requires bitcoin.conf to know how to connect to the RPC interface.

```
blocknotify=/some/path/ifpguard/ifpguard.py --bitcoind-config-file=/opt/bitcoind/bitcoin.conf
```

### Testnet example

```
testnet=1
blocknotify=/some/path/ifpguard/ifpguard.py --testnet
```

### Regtest example

```
regtest=1
blocknotify=/some/path/ifpguard/ifpguard.py --regtest
```

### Logging

IFPGuard will log to stdout. In addition, you can pass a path to a log file
with `--logfile`.

## Tests

Run `test/run_tests.py <path to bitcoind>`.

## Limitations

As implemented, the script checks if the first block has an output to
Bitcoin ABC (bitcoincash:pqnqv9lt7e5vjyp0w88zf2af0l92l8rxdgnlxww9j9).

The script does not verify if the amount is greater or equal to 8% of the
block reward.

## URSF (User Rejected Soft Fork)

If you want run this script on a non-mining node to show your support, you can
also add `uacomment=URSF-NOIFP` to you config file.

Adding the "URSF-NOIFP" comment to you nodes user agent allows it to be
searchable on https://cashnodes.io/nodes

## License

```
Copyright (C) 2020 Bitcoin Cash developers
Distributed under the MIT software license, see the accompanying
file COPYING or http://www.opensource.org/licenses/mit-license.php.
```
